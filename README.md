# iOS-UI-Development-with-Visual-Tools
Lynda Course | autolayout | UIStackView
iOS has visual UI development tools built right into Xcode, the IDE that iOS developers use the most. Xcode's Interface Builder provides graphical tools for manipulating layouts and views—which means little to no programming required. Interface design is a great way to break into iOS app development, or expand your programming skills to the realm of UX.
#Topics include:
Creating and customizing views
Laying out a user interface
Working with dynamic Auto Layout designs
Stacking views
Creating an adaptive layout
Handling orientation changes
Working with images
Customizing fonts
